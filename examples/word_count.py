#!/usr/bin/env python
# -*- coding:utf-8 -*-

import re


class MapReduce(object):

    @classmethod
    def map(cls, file, output):
        buffer = []
        with open(file) as f:
            for line in f:
                for word in line.split():
                    buffer.append(re.sub('[^0-9a-zA-Z]+', '', word).lower())
        buffer.sort()
        with open(output, 'w') as f:
            for i in buffer:
                f.write(i + ":" + "1" + "\n")

    @classmethod
    def reduce(cls, intermediate_pairs, output):
        buffer = {}
        with open(intermediate_pairs) as f:
            for line in f:
                word = line.split(":")[0]
                if buffer.has_key(word):
                    buffer[word] += 1
                else:
                    buffer[word] = 1
        with open(output, 'w') as f:
            for key, value in buffer.iteritems():
                f.write(key + ":" + str(value) + "\n")

if __name__ == '__main__':
    MapReduce.map("../input/input.txt", "map_output.txt")
    MapReduce.reduce("map_output.txt", "reduce_output.txt")