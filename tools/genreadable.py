#!/usr/bin/python

import sys
import random

'''
Random text files generator

Requirements: Unix based OS

Usage:
./genreadable.py <number of lines> <number of words in line> <output filename> <number of files> <increment lines for next files>
'''

OUTPUT_FOLDER = "../input/"

words = []

with open("/usr/share/dict/words") as inputs:
    for line in inputs:
        words.append(line.strip('\n'))

words_length = len(words)


def get_random_word():
    return words[random.randrange(0, words_length)]


def reprint(string):
    sys.stdout.write('%s\r' % string)
    sys.stdout.flush()

if len(sys.argv) != 6:
    print "Not enough arguments!"
    sys.exit()

number_of_lines = int(sys.argv[1])
number_of_words_per_line = int(sys.argv[2])
output_filename = str(sys.argv[3])
number_of_files = int(sys.argv[4])
line_increment = int(sys.argv[5])

for i in range(number_of_files):
    with open(OUTPUT_FOLDER + output_filename + str(i) + ".txt", "a") as output:
        for line_num in range(number_of_lines):
            for word_num in range(number_of_words_per_line):
                if word_num == number_of_words_per_line-1:
                    output.write(get_random_word())
                else:
                    output.write(get_random_word() + " ")
            output.write("\n")
    reprint(" Progress: %d of %d - %d%%" % (i+1, number_of_files, ((i+1)*100)/number_of_files))
    number_of_lines += line_increment

print "Generating files done."
