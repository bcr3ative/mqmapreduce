#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from glob import glob

from core.spec.mapreduce_input import MapReduceInput
from core.spec.mapreduce_output import MapReduceOutput
from core.spec.mapreduce_spec import MapReduceSpecification
from core.supervisor import Supervisor

spec = MapReduceSpecification()

if str(sys.argv[1]) == "-d":
    if len(glob(str(sys.argv[2] + "*"))) == 0:
        print "Incorrect path or directory is empty."
        sys.exit()

    for i in glob(str(sys.argv[2] + "*")):
        input_file = MapReduceInput(i, "text", "map")
        spec.add_input(input_file)
else:
    for i in range(1, len(sys.argv)):
        input_file = MapReduceInput(sys.argv[i], "text", "map")
        spec.add_input(input_file)


output_spec = MapReduceOutput("./output", "text", 2, "reduce")
spec.set_output(output_spec)

spec.set_workers(3)
spec.set_map_megabytes(16)
spec.set_reduce_megabytes(16)

supervisor = Supervisor(spec)
supervisor.run()
