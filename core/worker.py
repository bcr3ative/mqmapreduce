#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
from datetime import datetime
from multiprocessing import Process, Event

import pika

from core.logging.logger import Logger
from examples.word_count import MapReduce


class Worker(Process):

    @classmethod
    def start_workers(cls, num):
        for i in range(num):
            process = Worker(i)
            process.start()

    def __init__(self, id_):
        super(Worker, self).__init__()
        self.id = id_
        self.log_tag = "worker" + str(id_)
        self.stop_working = Event()

    def run(self):
        self.__establish_connection()
        self.__setup_communication()

        """Handshake"""
        self.channel.basic_publish(exchange='workers',
                                   routing_key='setup',
                                   body=Messages.handshake(self.id))

        while not self.stop_working.is_set():
            self.connection.process_data_events()

        self.__close_connection()

    def signal_exit(self):
        self.stop_working.set()

    def __on_message_received(self, channel, method, properties, body):
        message = json.loads(body)

        if message['operation'] == 'map':
            file_name = message['file']
            map_id = int(message['map'])
            start_time = datetime.now()
            MapReduce.map(file_name, "./output/map_output" + str(map_id) + ".txt")
            Logger.log_to_file(self.log_tag, "MAP"+str(map_id)+"|"+str(datetime.now() - start_time))
            message = Messages.map_finished("./output/map_output" + str(map_id) + ".txt", self.id, map_id)
            self.channel.basic_publish(exchange='workers', routing_key='result', body=message)

        elif message['operation'] == 'reduce':
            intermediate_pairs = message['file']
            reduce_id = int(message['reduce'])
            start_time = datetime.now()
            MapReduce.reduce(intermediate_pairs, "./output/reduce_output" + str(reduce_id) + ".txt")
            Logger.log_to_file(self.log_tag, "REDUCE"+str(reduce_id)+"|"+str(datetime.now() - start_time))
            message = Messages.reduce_finished(self.id, reduce_id)
            self.channel.basic_publish(exchange='workers', routing_key='result', body=message)

    def __establish_connection(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()

    def __close_connection(self):
        self.channel.stop_consuming()
        self.connection.close()

    def __setup_communication(self):
        """Exchange from where the supervisor monitors the workers"""
        self.channel.exchange_declare(exchange='supervisor',
                                      type='direct')

        result = self.channel.queue_declare(exclusive=True)
        queue_name = result.method.queue

        self.channel.queue_bind(exchange='supervisor',
                                queue=queue_name,
                                routing_key=str(self.id))

        self.channel.basic_consume(self.__on_message_received,
                                   queue=queue_name,
                                   no_ack=True)

        """Exchange from where the workers send messages to the supervisor"""
        self.channel.exchange_declare(exchange='workers',
                                      type='direct')


class Messages(object):

    @classmethod
    def handshake(cls, id_):
        message = {'worker': str(id_),
                   'content': 'hello'}
        return json.dumps(message)

    @classmethod
    def map_finished(cls, output, worker_id, map_id):
        message = {'content': 'map_finished',
                   'output': output,
                   'worker': str(worker_id),
                   'map': str(map_id)}
        return json.dumps(message)

    @classmethod
    def reduce_finished(cls, worker_id, reduce_id):
        message = {'content': 'reduce_finished',
                   'worker': str(worker_id),
                   'reduce': str(reduce_id)}
        return json.dumps(message)
