#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
from datetime import datetime
import math
import curses

import pika

from core.data_structures import WorkerInfo, MapInfo, ReduceInfo
from core.states import WorkerStates, TaskStates
from core.worker import Worker
from core.logging.logger import Logger


class Supervisor(object):

    def __init__(self, spec):
        self.spec = spec
        self.workers = {}
        self.maps = {}
        self.maps_count = 0
        self.reduces = {}
        self.reduces_count = 0
        self.log_tag = "supervisor"
        self.start_time = datetime.now()
        self.stdscr = curses.initscr()

        for i in range(self.spec.get_workers_num()):
            worker = WorkerInfo(i)
            worker.set_state(WorkerStates.NOT_STARTED)
            self.workers[i] = worker

        for i in range(self.spec.get_input_num()):
            map_ = MapInfo(i)
            map_.set_file_path(self.spec.input_files_spec[i].get_file_name())
            map_.set_state(TaskStates.IDLE)
            self.maps[i] = map_

        #for i in range(self.spec.output_spec.get_num_tasks()):
        for i in range(self.spec.get_input_num()):
            reduce_ = ReduceInfo(i)
            reduce_.set_state(TaskStates.IDLE)
            self.reduces[i] = reduce_

    def __del__(self):
        self.connection.close()

    def run(self):
        self.__establish_connection()
        self.__setup_communication()

        self.__start_workers()

        Logger.log_to_file(self.log_tag, "Listening...")
        self.__refresh_status()
        self.channel.start_consuming()

    def __establish_connection(self):
        Logger.log_to_file(self.log_tag, "Creating connection...")
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()

    def __setup_communication(self):
        Logger.log_to_file(self.log_tag, "Setting up architecture...")
        # Exchange from where the supervisor monitors the workers
        self.channel.exchange_declare(exchange='supervisor',
                                      type='direct')

        # Exchange from where the workers send messages to the supervisor
        self.channel.exchange_declare(exchange='workers',
                                      type='direct')

        self.channel.queue_declare(queue='setup')
        self.channel.queue_declare(queue='status')
        self.channel.queue_declare(queue='result')

        # Setup queue
        self.channel.queue_bind(exchange='workers',
                                queue='setup',
                                routing_key='setup')

        self.channel.basic_consume(self.__setup_callback,
                                   queue='setup',
                                   no_ack=True)

        # Status queue
        self.channel.queue_bind(exchange='workers',
                                queue='status',
                                routing_key='status')

        self.channel.basic_consume(self.__status_callback,
                                   queue='status',
                                   no_ack=True)

        # Result queue
        self.channel.queue_bind(exchange='workers',
                                queue='result',
                                routing_key='result')

        self.channel.basic_consume(self.__result_callback,
                                   queue='result',
                                   no_ack=True)

    def __start_workers(self):
        Worker.start_workers(self.spec.get_workers_num())

    def __setup_callback(self, channel, method, properties, body):
        message = json.loads(body)
        if message['content'] == 'hello':
            worker_id = int(message['worker'])
            Logger.log_to_file(self.log_tag, "Worker "+str(worker_id)+" said hello!")
            self.workers[worker_id].set_state(WorkerStates.IDLE)

            map_id = self.__get_next_idle_map()
            if map_id != -1:
                Logger.log_to_file(self.log_tag, "Worker "+str(worker_id)+" got map "+str(map_id)+" for execution!")
                file_path = self.maps[map_id].get_file_path()
                self.maps[map_id].set_assigned_worker(worker_id)
                self.maps[map_id].set_state(TaskStates.IN_PROGRESS)
                self.maps[map_id].set_start_time(datetime.now())
                message = Messages.map_task(file_path, map_id)
                self.channel.basic_publish(exchange='supervisor', routing_key=str(worker_id), body=message)
                self.workers[worker_id].set_state(WorkerStates.ACTIVE)

    def __status_callback(self, channel, method, properties, body):
        pass

    def __result_callback(self, channel, method, properties, body):
        message = json.loads(body)
        worker_id = int(message['worker'])

        if message['content'] == 'map_finished':
            map_id = int(message['map'])
            Logger.log_to_file(self.log_tag, "Worker "+str(worker_id)+" finished with the execution of map "+str(map_id)+"!")
            file_path = message['output']
            self.workers[worker_id].set_state(WorkerStates.IDLE)
            self.maps[map_id].set_state(TaskStates.COMPLETED)
            self.maps[map_id].set_end_time(datetime.now())
            self.maps_count += 1
            self.reduces[map_id].set_file_path(file_path)
            self.__refresh_status()

        elif message['content'] == 'reduce_finished':
            reduce_id = int(message['reduce'])
            Logger.log_to_file(self.log_tag, "Worker "+str(worker_id)+" finished with the execution of reduce "+str(reduce_id)+"!")
            self.workers[worker_id].set_state(WorkerStates.IDLE)
            self.reduces[reduce_id].set_state(TaskStates.COMPLETED)
            self.reduces[reduce_id].set_end_time(datetime.now())
            self.reduces_count += 1
            self.__refresh_status()

        idle_map = self.__get_next_idle_map()
        if idle_map != -1:
            Logger.log_to_file(self.log_tag, "Worker "+str(worker_id)+" got map "+str(idle_map)+" for execution!")
            file_path = self.maps[idle_map].get_file_path()
            self.maps[idle_map].set_assigned_worker(worker_id)
            self.maps[idle_map].set_state(TaskStates.IN_PROGRESS)
            self.maps[idle_map].set_start_time(datetime.now())
            message = Messages.map_task(file_path, idle_map)
            self.channel.basic_publish(exchange='supervisor', routing_key=str(worker_id), body=message)
            self.workers[worker_id].set_state(WorkerStates.ACTIVE)
            return

        idle_reduce = self.__get_next_idle_reduce()
        if idle_reduce != -1:
            Logger.log_to_file(self.log_tag, "Worker "+str(worker_id)+" got reduce "+str(idle_reduce)+" for execution!")
            file_path = self.reduces[idle_reduce].get_file_path()
            self.reduces[idle_reduce].set_assigned_worker(worker_id)
            self.reduces[idle_reduce].set_state(TaskStates.IN_PROGRESS)
            self.reduces[idle_reduce].set_start_time(datetime.now())
            message = Messages.reduce_task(file_path, idle_reduce)
            self.channel.basic_publish(exchange='supervisor', routing_key=str(worker_id), body=message)
            self.workers[worker_id].set_state(WorkerStates.ACTIVE)
            return

    def __get_next_idle_map(self):
        for i in range(self.spec.get_input_num()):
            if self.maps[i].get_state() == TaskStates.IDLE:
                return i
        return -1

    def __get_next_idle_reduce(self):
        for i in range(self.spec.get_input_num()):
            if self.reduces[i].get_state() == TaskStates.IDLE and self.reduces[i].file_path is not None:
                return i
        return -1

    def __refresh_status(self):
        start = self.start_time
        current = datetime.now()
        uptime = current - start
        map_percentage = int(math.floor((float(self.maps_count) / self.spec.get_input_num()) * 100))
        reduce_percentage = int(math.floor((float(self.reduces_count) / self.spec.get_input_num()) * 100))
        self.stdscr.addstr(0, 0, "StartTime: {0}, CurrentTime: {1}, Uptime: {2}".format(start, current, uptime))
        self.stdscr.addstr(1, 0, "WorkersEmployed: {0}".format(self.spec.get_workers_num()))
        self.stdscr.addstr(2, 0, "Tasks: {0}, MapTasks: {1}, ReduceTasks: {2}".format(self.spec.get_input_num() * 2, self.spec.get_input_num(), self.spec.get_input_num()))
        self.stdscr.addstr(3, 0, "MapStatus: [{1:20}] {0}%".format(map_percentage, "#" * (map_percentage/5)))
        self.stdscr.addstr(4, 0, "RedStatus: [{1:20}] {0}%".format(reduce_percentage, "#" * (reduce_percentage/5)))
        self.stdscr.refresh()


class Messages(object):

    @classmethod
    def map_task(cls, file_path, map_id):
        message = {'operation': 'map',
                   'file': file_path,
                   'map': map_id,
                   'mapperclass': 'a'}
        return json.dumps(message)

    @classmethod
    def reduce_task(cls, file_path, reduce_id):
        message = {'operation': 'reduce',
                   'file': file_path,
                   'reduce': reduce_id,
                   'mapperclass': 'a'}
        return json.dumps(message)
