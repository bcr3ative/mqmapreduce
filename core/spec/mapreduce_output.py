#!/usr/bin/env python
# -*- coding:utf-8 -*-


class MapReduceOutput(object):

    def __init__(self, output_dir, file_format, num_tasks, reducer_class):
        self.output_dir = output_dir
        self.file_format = file_format
        self.num_tasks = num_tasks
        self.reducer_class = reducer_class

    def get_num_tasks(self):
        return self.num_tasks