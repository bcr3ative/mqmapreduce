#!/usr/bin/env python
# -*- coding:utf-8 -*-


class MapReduceInput(object):

    def __init__(self, file_name, file_format, mapper_class):
        self.file_name = file_name
        self.file_format = file_format
        self.mapper_class = mapper_class

    def get_file_name(self):
        return self.file_name

    def get_mapper_class(self):
        return self.mapper_class