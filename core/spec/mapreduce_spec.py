#!/usr/bin/env python
# -*- coding:utf-8 -*-


class MapReduceSpecification(object):

    def __init__(self):
        self.input_files_spec = []
        self.output_spec = None

        self.workers = None
        self.map_megabytes = None
        self.reduce_megabytes = None

    def add_input(self, spec):
        self.input_files_spec.append(spec)

    def set_output(self, spec):
        self.output_spec = spec

    def set_workers(self, num=10):
        self.workers = num

    def set_map_megabytes(self, mb=16):
        self.map_megabytes = mb

    def set_reduce_megabytes(self, mb=16):
        self.reduce_megabytes = mb

    def get_input_num(self):
        return len(self.input_files_spec)

    def get_workers_num(self):
        return self.workers
