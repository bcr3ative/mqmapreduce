#!/usr/bin/env python
# -*- coding:utf-8 -*-


class Info(object):

    def __init__(self, id_):
        self.id_ = id_
        self.state = None

    def get_id(self):
        return self.id_

    def get_state(self):
        return self.state

    def set_state(self, state):
        self.state = state


class WorkerInfo(Info):

    def __init__(self, id_):
        super(WorkerInfo, self).__init__(id_)


class MapInfo(Info):

    def __init__(self, id_):
        super(MapInfo, self).__init__(id_)
        self.assigned_worker = None
        self.file_path = None
        self.start_time = None
        self.end_time = None

    def get_assigned_worker(self):
        return self.assigned_worker

    def set_assigned_worker(self, worker):
        self.assigned_worker = worker

    def get_file_path(self):
        return self.file_path

    def set_file_path(self, path):
        self.file_path = path

    def set_start_time(self, time):
        self.start_time = time

    def get_start_time(self):
        return self.start_time

    def set_end_time(self, time):
        self.end_time = time

    def get_end_time(self):
        return self.end_time


class ReduceInfo(Info):

    def __init__(self, id_):
        super(ReduceInfo, self).__init__(id_)
        self.assigned_worker = None
        self.file_path = None
        self.start_time = None
        self.end_time = None

    def get_assigned_worker(self):
        return self.assigned_worker

    def set_assigned_worker(self, worker):
        self.assigned_worker = worker

    def get_file_path(self):
        return self.file_path

    def set_file_path(self, path):
        self.file_path = path

    def set_start_time(self, time):
        self.start_time = time

    def get_start_time(self):
        return self.start_time

    def set_end_time(self, time):
        self.end_time = time

    def get_end_time(self):
        return self.end_time
