#!/usr/bin/env python
# -*- coding:utf-8 -*-


class TaskStates(object):

    IDLE = 0
    IN_PROGRESS = 1
    COMPLETED = 2


class WorkerStates(object):

    NOT_STARTED = 0
    IDLE = 1
    ACTIVE = 2
    NOT_RESPONDING = 3
