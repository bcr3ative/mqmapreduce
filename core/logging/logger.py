#!/usr/bin/env python
# -*- coding:utf-8 -*-

from datetime import datetime

# Relative to run_supervisor.py
LOG_FOLDER = "./logs/"


class Logger(object):

    @classmethod
    def form_log_prefix(cls, log_tag):
        prefix = "[" + str(datetime.now()) + "][" + log_tag + "]->"
        return prefix

    @classmethod
    def log_to_file(cls, log_tag, log_msg):
        with open(LOG_FOLDER + log_tag + ".log", mode='a') as f:
            f.write(Logger.form_log_prefix(log_tag)+log_msg)
            f.write("\n")

    @classmethod
    def log_to_console(cls, log_tag, log_msg):
        print Logger.form_log_prefix(log_tag)+log_msg
